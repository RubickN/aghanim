from whoosh.fields import TEXT, ID, Schema
from whoosh.filedb.filestore import FileStorage
from whoosh.qparser import QueryParser
from jieba.analyse import ChineseAnalyzer
from whoosh.index import create_in
from logger import logger
from core.file_manager import Document
import os
import shutil


class SearchEngine(object):

    def __init__(self, index_dir):
        self.whoosh_index = None
        self.analyzer = ChineseAnalyzer()
        self.schema = Schema(
            resource_id=TEXT(stored=True),
            title=TEXT(stored=True),
            date=TEXT(stored=True),
            content=TEXT(stored=True, analyzer=self.analyzer),
        )
        self.index_dir = index_dir
        self.init()

    def all_resource(self):
        return self.whoosh_index.reader().all_stored_fields()

    def all_resource_id(self):
        return [i['resource_id'] for i in self.all_resource()]

    def init(self):
        """
        初始化文档搜索引擎的结构目录
        :return:
        """
        if os.path.exists(self.index_dir) is False:
            os.mkdir(self.index_dir)
            self.whoosh_index = create_in(self.index_dir, self.schema)
            # self.loads()
        else:
            try:
                storage = FileStorage(self.index_dir)
                self.whoosh_index = storage.open_index(schema=self.schema)
            except Exception as e:
                print(e)
                os.remove(self.index_dir)
                shutil.rmtree(self.index_dir)
                self.init()

    def loads(self, files):
        """
        :param documents: [files]
        :return:
        """
        writer = self.whoosh_index.writer()
        for file in files:
            if file.id in self.all_resource_id():
                continue
            logger.info('开始导入: %s' % str(file.name))
            writer.add_document(
                resource_id=file.id,
                title=file.name,
                date=file.create_date,
                content=file.content
            )
        writer.commit()

    def search(self, keyword):
        searcher = self.whoosh_index.searcher()
        parser = QueryParser("content", schema=self.schema)
        q = parser.parse(keyword)
        results = searcher.search(q, limit=99999, collapse_limit=5)
        results.fragmenter.charlimit = None
        results.fragmenter.maxchars = 99999
        results.fragmenter.surround = 99999
        return_result = []
        for hit in results:
            return_result.append(
                Document(**{
                    "title": hit['title'],
                    "date": hit['date'],
                    "content": hit.highlights("content", top=99999),
                    "id": hit['resource_id']
                })
            )
        return return_result