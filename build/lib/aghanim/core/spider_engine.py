

class HtmlPage(object):
    def __init__(self,
                 id,
                 content,
                 date,
                 title):
        self.id = id
        self.content = content
        self.date = date
        self.title = title


class Spider(object):
    def run(self):
        return []


class SpiderNat(object):
    def __init__(self):
        self.nat = []

    def run(self):
        result = []
        for spider in self.nat:
            result.extend(spider.run())
        return result

    def register_spider(self, spider):
        self.nat.append(spider)