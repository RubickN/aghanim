from core.file_manager import FileModel, write_file, read_file
import os


class BaseResource(object):
    spider = None

    def __init__(self, resource_dir):
        self.resource_dir = resource_dir
        self.init_resource_dir()

    def get_all_resource(self):
        files = []
        for _dir, _n, _files in os.walk(self.resource_dir):
            for _file in _files:
                file = FileModel.gen(self.resource_dir, _file)
                files.append(file)
        return files

    @property
    def all_ids(self):
        return [resource.id for resource in self.get_all_resource()]

    def init_resource_dir(self):
        if os.path.exists(self.resource_dir) is False:
            os.mkdir(self.resource_dir)