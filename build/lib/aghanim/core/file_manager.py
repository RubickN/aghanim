import os
import uuid
import codecs
import datetime
import re


def write_file(obj):
    """
    保存文件对象
    :param obj:
    :return:
    """
    fullpath = os.path.join(obj.path, obj.filename)
    with codecs.open(fullpath, 'w', encoding='utf_8_sig') as f:
        f.write(obj.content)


def read_file(path):
    """
    读取文件内容
    :param obj:
    :return:
    """
    with codecs.open(path, 'rb', encoding='utf_8_sig') as f:
        data = f.read()
    return data


class FileModel(object):
    FILENAME_FORMAT = "{create_date}-{name}_{id}.{file_type}"

    def __init__(self,
                 path,
                 name,
                 file_type,
                 content,
                 id=None,
                 create_date=None):
        self.path = path
        self.name = name
        self.file_type = file_type
        self.content = content
        self.id = id or self.gen_id()
        self.create_date = create_date or self.gen_create_date()

    @property
    def utf8_content(self):
        return self.content.decode("utf-8")

    @staticmethod
    def gen_id():
        return uuid.uuid4().hex

    @staticmethod
    def gen_create_date():
        return datetime.datetime.now().strftime("%Y-%m-%d")

    @classmethod
    def gen(cls, path, filename):
        full_path = os.path.join(path, filename)
        content = read_file(full_path)
        file_params = {
            "path": path,
            "content": content,
            **cls.parse_filename(filename, cls.FILENAME_FORMAT)
        }
        return cls(**file_params)

    @staticmethod
    def parse_filename(filename, _format):
        """
        解析一个文件名中包含的内容
        :param filename:
        :return:
        """
        keys = re.findall("\{(\S+?)\}", _format)
        for _k in keys:
            _format = _format.replace("{%s}" % _k, "(.*)")
        _format = _format.replace(").", ")\.")
        values = re.search(_format, filename).groups()
        return dict(zip(keys, values))

    @property
    def filename(self):
        return self.FILENAME_FORMAT.format(
            create_date=self.create_date,
            name=self.name,
            id=self.id,
            file_type=self.file_type
        )


class Document(object):
    def __init__(self,
                 id,
                 content,
                 date,
                 title):
        self.id = id
        self.content = content
        self.date = date
        self.title = title

    @property
    def utf8_content(self):
        return self.content.decode('utf-8')