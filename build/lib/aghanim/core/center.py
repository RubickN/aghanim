class Center(object):
    def __init__(self):
        self.search_engine = None
        self.resource = None

    def update_resource(self):
        self.resource.update_resource()

    def search(self, key_word):
        return self.search_engine.search(key_word)

    def loads(self):
        self.search_engine.loads(self.resource.get_all_resource())
