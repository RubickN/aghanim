import os


class NetConst(object):
    HOME_URL = 'http://www.mutongzixun.com'
    PAGE_URL = 'http://www.mutongzixun.com/index.php?m=content&c=index&a=lists&catid=8'
    STANDARD_HEAD = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, compress',
            'Accept-Language': 'en-us;q=0.5,en;q=0.3',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0'
        }


class BaseConfiguration(object):
    CURRENT_DIR = os.path.dirname(__file__)
    RESOURCE_FILENAME = 'resource'
    RESOURCE_FILE = os.path.join(CURRENT_DIR, RESOURCE_FILENAME)
    RESULT_FILENAME = 'result'
    CACHE_FILENAME = 'cache_lib'
    CACHE_FILE = os.path.join(CURRENT_DIR, CACHE_FILENAME)
    WHOOSH_INDEX_FILENAME = 'index_dir'
    WHOOSH_INDEX_FILE = os.path.join(CURRENT_DIR, WHOOSH_INDEX_FILENAME)
    RESULT_HTML_DIR = os.path.join(CURRENT_DIR, 'results')
    INTERFACE_CACHE_DIR = 'interface_cache'
    INTERFACE_CACHE = os.path.join(CURRENT_DIR, INTERFACE_CACHE_DIR)
    CSS_STYLE = "<style style='text/css'>b{color:black;background-color:red}</style>"
    REDIS_CACHE_SERVER = '192.168.139.137'
