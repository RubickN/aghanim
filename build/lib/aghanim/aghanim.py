from mutong.center import MuTongCenter


class CenterManager(object):
    def __init__(self):
        self.registry = {
            'mutong': MuTongCenter()
        }

    def update_resource(self, resource_name):
        self.registry[resource_name].update_resource()

    def search(self, key_word, resource_name):
        return self.registry[resource_name].search(key_word)

    def loads(self, resource_name):
        return self.registry[resource_name].loads()

    def all_update_resource(self):
        for center in self.registry.values():
            center.update_resource()

    def search_all(self, key_word):
        res = []
        for center in self.registry.values():
            res.extend(center.search(key_word))
        return res


if __name__ == "__main__":
    # print(CenterManager().loads("mutong"))
    print(CenterManager().search_all("煤炭"))
    # print(CenterManager().update_resource("mutong"))