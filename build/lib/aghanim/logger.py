import logging
import os
import time


class Logger(object):
    level_relations = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'crit': logging.CRITICAL
    }

    def __init__(self, level='info', when='D', backCount=3,
                 fmt='%(asctime)s-[%(filename)s:%(lineno)d]-%(levelname)s: %(message)s'):
        rq = time.strftime('%Y%m%d', time.localtime(time.time()))
        log_path = os.getcwd() + '/Logs/'
        if os.path.exists(log_path) is False:
            os.mkdir(log_path)
        filename = log_path + rq + '.log'
        self.logger = logging.getLogger(filename)
        self.logger.setLevel(self.level_relations.get(level))
        format_str = logging.Formatter(fmt)

        fh = logging.FileHandler(filename, mode='a')
        fh.setLevel(logging.INFO)  # 输出到file的log等级的开关
        fh.setFormatter(format_str)

        sh = logging.StreamHandler()
        sh.setFormatter(format_str)

        self.logger.addHandler(sh)
        self.logger.addHandler(fh)

    def get_logger(self):
        return self.logger


logger = Logger().get_logger()