from untils import request_get, logger
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
from core.spider_engine import Spider, HtmlPage
import re


class MuTongSpider(Spider):
    def __init__(self,
                 main_page_url,
                 home_page_url):
        self.main_page_url = main_page_url
        self.home_page_url = home_page_url

    def get_page_nums(self):
        """
        获取总共需要爬取多少页
        :return:
        """
        url = self.main_page_url
        html_detail = request_get(url)
        pages_div = BeautifulSoup(html_detail, "html.parser").find_all('li', attrs={'class': 'hidden-xs hidden-sm'})
        nums = re.findall(">(\d+)", str(pages_div))[-1]
        logger.info('需要爬取共 %s 页' % nums)
        return nums

    def _create_pages_url(self):
        """
        生成需要爬取的页面URL
        :return:
        """
        url = self.main_page_url
        total_page_num = self.get_page_nums()
        links = ['%s&page=%d' % (url, num) for num in range(1, int(total_page_num) + 1)]
        return links

    def get_summary_content(self, link):
        """
        解析概括页面
        :param link:
        :return:
        """

        home_page_url = self.home_page_url

        content_html = request_get(link)
        pages_div = BeautifulSoup(content_html, "html.parser").find_all('div', attrs={'class': 'col-md-12 mod-main-block'})
        results = []
        for page in pages_div:
            title = page.find_all('a', attrs={'target': '_blank'})[0].string
            content_link = page.find_all('a', attrs={'target': '_blank'})[0].get('href')
            time_div = page.find_all('p', attrs={'class': 'mod-main-date text-muted'})[0]
            time = re.search('(\d+-\d+-\d+ \d+:\d+:\d+)', str(time_div)).groups()[0]
            _id = re.search('&id=(\d+)', content_link).groups()[0]
            results.append([title, home_page_url + content_link, time, _id])
        return results

    def main_get_summary_page(self):
        """
        爬取目录页面
        :return:
        """
        links = self._create_pages_url()
        link_pool = ThreadPool(3)
        logger.info('开始爬取目录页面')
        results = []
        content_links = []
        for each_link in links:
            results.append(link_pool.apply_async(self.get_summary_content, (each_link, )))
        for i in results:
            i.wait()
            content_links.extend(i.get())
        link_pool.close()
        link_pool.join()
        return content_links

    def get_detail_content(self, link):
        try:
            """
            解析主界面，去除广告
            :param link:
            :return:
            """
            url = link[1]
            content_html = request_get(url)
            content = BeautifulSoup(content_html, "html.parser").find_all('div',
                                                                          attrs={'class': 'row mod-main-list'})[0]
            link.append(str(content))
            return link
        except Exception as e:
            logger.critical('解析主界面遇到未知错误，跳过该链接， %s 标题：%s' % (e, link[0]))
            link.append('')
            return link

    def main_get_detail_page(self, no_include_ids=[]):
        """
        爬取每一个链接里的网页
        :return:
        """
        links = self.main_get_summary_page()
        link_pool = ThreadPool(3)
        results = []
        content_details = []
        for each_link in links:
            id = each_link[3]
            if id in no_include_ids:
                continue
            logger.info("抓取:%s, url:%s" % (each_link[0], each_link[1]))
            results.append(link_pool.apply_async(self.get_detail_content, (each_link, )))
        for i in results:
            i.wait()
            result = i.get()
            html_page = HtmlPage(
                id=result[3],
                date=result[2],
                content=result[4],
                title=result[0]
            )
            content_details.append(html_page)
        link_pool.close()
        link_pool.join()
        return content_details

    def run(self, no_include_ids=[]):
        contents = self.main_get_detail_page(no_include_ids)
        return contents