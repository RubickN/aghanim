from setuptools import setup, find_packages

setup(
    name="aghanim",
    packages=find_packages(exclude=[]),
    packages_data={"": ["*.*"]},
    classifiers=[
        "Programming Language :: Python",
        "Operating System :: Unix",
        "Programming Language :: Python :: 3.6",
    ]
)