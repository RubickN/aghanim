from aghanim.core.search_engine import SearchEngine
from aghanim.core.resource import BaseResource
from aghanim.core.file_manager import FileModel, write_file
from aghanim.core.center import Center
from aghanim.mutong.mutong_conf import NetConst, BaseConfiguration
from aghanim.mutong.mutong_spider import MuTongSpider


class MuTongResource(BaseResource):
    def __init__(self, resource_dir):
        super(MuTongResource, self).__init__(resource_dir)
        self.spider = MuTongSpider(NetConst.PAGE_URL,
                                   NetConst.HOME_URL)
        self.resource_dir = resource_dir

    def update_resource(self):
        html_pages = self.spider.run(self.all_ids)
        for html_page in html_pages:
            file_model = FileModel(
                path=self.resource_dir,
                name=html_page.title,
                file_type="html",
                create_date=html_page.date[:10],
                content=html_page.content,
                id=html_page.id
            )
            write_file(file_model)


class MuTongCenter(Center):
    def __init__(self):
        self.search_engine = SearchEngine(BaseConfiguration.WHOOSH_INDEX_FILE)
        self.resource = MuTongResource(BaseConfiguration.RESOURCE_FILE)

