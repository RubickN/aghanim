from aghanim.conf import NetConst
from aghanim.logger import logger
import requests


class CustomException(object):
    def __init__(self, _str):
        self._str = _str

    def __repr__(self):
        return self._standard_output

    def __index__(self):
        return self._standard_output

    @property
    def _standard_output(self):
        return self._str


class NetworkException(CustomException, TimeoutError):
    pass


def request_get(url):
    # 发送get请求
    heads = NetConst.STANDARD_HEAD
    try:
        data = requests.get(url, headers=heads)
        return data.text
    except NetworkException as e:
        logger.critical('网络链接失败')
    except Exception as e:
        logger.exception('请求遇到未知异常，具体信息:%s' % e)