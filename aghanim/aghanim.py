from aghanim.mutong.center import MuTongCenter


class CenterManager(object):
    def __init__(self):
        self.registry = {
            'mutong': MuTongCenter()
        }

    def update_resource(self, resource_name):
        self.registry[resource_name].update_resource()

    def search(self, key_word, resource_name):
        return self.registry[resource_name].search(key_word)

    def loads(self, resource_name):
        return self.registry[resource_name].loads()

    def update_all_resource(self):
        for center in self.registry.values():
            center.update_resource()

    def search_all(self, key_word):
        res = []
        for center in self.registry.values():
            res.extend(center.search(key_word))
        return res

    def get_resource_nums(self, resource_name):
        return self.registry[resource_name].loaded_resource_nums

    def get_all_resource_nums(self):
        res = 0
        for center in self.registry.values():
            res += center.loaded_resource_nums
        return res

    def get_latest_date(self):
        return list(self.registry.values())[0].latest_date

    def get_content(self, id):
        res = None
        for center in self.registry.values():
            res = center.get_content(id)
            if res is not None:
                break
        return res


if __name__ == "__main__":
    # print(CenterManager().loads("mutong"))
    print(CenterManager().search_all("煤炭"))
    # print(CenterManager().update_resource("mutong"))